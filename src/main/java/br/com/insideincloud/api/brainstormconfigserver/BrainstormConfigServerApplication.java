package br.com.insideincloud.api.brainstormconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class BrainstormConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrainstormConfigServerApplication.class, args);
	}

}
